#!/usr/bin/env python3

import sys
def banner():
	print("$$\       $$\            $$$$$$\                               $$\ $$\ ")
	print("$$ |      \__|          $$  __$$\                              \__|\__|")
	print("$$$$$$$\  $$\ $$$$$$$\  \__/  $$ |$$$$$$\   $$$$$$$\  $$$$$$$\ $$\ $$\ ")
	print("$$  __$$\ $$ |$$  __$$\  $$$$$$  |\____$$\ $$  _____|$$  _____|$$ |$$ |")
	print("$$ |  $$ |$$ |$$ |  $$ |$$  ____/ $$$$$$$ |\$$$$$$\  $$ /      $$ |$$ |")
	print("$$ |  $$ |$$ |$$ |  $$ |$$ |     $$  __$$ | \____$$\ $$ |      $$ |$$ |")
	print("$$$$$$$  |$$ |$$ |  $$ |$$$$$$$$\\$$$$$$$ |$$$$$$$  |\$$$$$$$$\ $$ |$$ |")
	print("\_______/ \__|\__|  \__|\________|\_______|\_______/  \_______|\__|\__|")
	print("\nbin2ascii 0.1")
	print("By Ash [https://gitlab.com/@sim/]")
	print("Try './bin2ascii -h' for help") 

def help():
	print('Usage: ./bin2ascii [Option] "input"')
	print("Options:\n\t-a or --ascii to convert from ASCII to binary.\n\t-b or --binary to convert from binary to ASCII\n\t-h or --help to show this help screen")

if __name__ == "__main__":
    if len(sys.argv) != 1 and len(sys.argv) <= 3:
        if sys.argv[1] == "-b" or sys.argv[1] == "--binary":
            n = int(sys.argv[2].replace(' ', ''), 2)
            print(n.to_bytes((n.bit_length() + 7) // 8, 'big').decode())
        elif sys.argv[1] == "-a" or sys.argv[1] == "--ascii":
            print((bin(int.from_bytes(sys.argv[2].encode(), 'big')))[2:])
        elif sys.argv[1] == "-h" or sys.argv[1] == "--help":
            help()
        else:
            print("\nEnter a valid command!")
            help()
    else:
         banner()
